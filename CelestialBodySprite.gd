extends Area2D


@onready var sprite := $Sprite2D
@onready var shape := $CollisionShape2D

var first_visit_dialogue: StringName
var visited_dialogue: StringName
var selected := false
var has_been_visited := false
var is_active := true


func _ready():
	SignalBus.dialogue_requested.connect((func(): is_active = false).unbind(1))
	SignalBus.dialogue_ended.connect(func(): is_active = true)
	connect("area_entered", _entered_area)
	connect("area_exited", _exited_area)


func set_data(data: CelestialBody):
	sprite.texture = data.texture
	shape.shape = data.shape
	first_visit_dialogue = data.first_visit_dialogue
	visited_dialogue = data.visited_dialogue


func select():
	selected = true
	sprite.modulate = "00FFFF"


func unselect():
	selected = false
	sprite.modulate = "FFFFFF"


func visit():
	SignalBus.body_visited.emit()
	SignalBus.dialogue_requested.emit(first_visit_dialogue)
	unselect()
	has_been_visited = true


func _entered_area(area_2D: Area2D) -> void:
	if area_2D.get_parent() is CharacterBody2D:
		select()

func _exited_area(area_2D: Area2D) -> void:
	if area_2D.get_parent() is CharacterBody2D:
		unselect()


func _input(event):
	if selected and is_active and event.is_action_pressed("select_body"):
		if not has_been_visited:
			visit()
		else:
			SignalBus.dialogue_requested.emit(visited_dialogue)
