Qué roca tan curiosa. Me recuerda a...
+ [Un caballo] -> caballo
+ [Una bicicleta] -> bicicleta
    
=== caballo ===
Es igualita a Azabache, mi viejo caballo.
Ay, qué buenas tardes echábamos mi padre y yo en el club de campo. Recuerdo una vez en la que por poco me quedo sin cabeza.
La yegua de mi padre se asustó y vino corriendo hacia mí, que estaba sentado en el suelo leyendo.
Por suerte, solo el libro salió mal parado.
-> END

=== bicicleta ===
Fijándome bien, tiene la forma de mi primera bici.
La heredé de mi hermano, que la heredó de mi primo, que probablemente la heredó de alguien que la robó de un vertedero.
Tenía los frenos destrozados, estaba medio doblada y era de un flamante naranja óxido, pero era mi bici, y me dio mucha pena tener que deshacerme de ella.
Al menos la acabó heredando mi sobrina.
-> END
