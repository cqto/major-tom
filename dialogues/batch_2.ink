Diario de a bordo. Día 122.

Han pasado tres días desde la última transmisión. No pintaba nada bien...
Aunque no pueda contactar con la Tierra, sí recibo sus comunicaciones. Miembros del equipo me mandan sus ánimos, sin saber siquiera si me llegan.
Me aseguran que hacen todo lo posible, que dentro de poco me encontrarán y podrán traerme de vuelta.
Hasta entonces, seguiré documentando mi viaje. Por ellos. 

Firmado,
+ Un náufrago[]
+ Un soñador[]