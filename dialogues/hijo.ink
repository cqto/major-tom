...
+ [¡Vienen los extraterrestres!] -> juego
+ [Mis ojos me traicionan] -> llanto

=== juego ===
¡Rápido, capitán! ¡Media vuelta!
El malvado ejército de extraterrestres malvados ha lanzado un ataque de... ¡Pelotas de tenis!
Debe ser la comandante mamá... ¿Qué deberíamos hacer, valiente capitán?
...
...
¿Capitán?
-> END

=== llanto ===
Es... el cohete que le hice a mi hijo.
Explorábamos el espacio exterior en el nuevo nuevísimo cohete Stardust, descubriendo nuevos mundos y luchando contra los extraterrestres.
A veces, la malvada comandante mamá capturaba la nave, exigiendo al capitán "los deberes". pero era tan valiente que los terminaba en seguida.
...
...
Lo echo de menos.
Lo echo tantísimo de menos...
-> END