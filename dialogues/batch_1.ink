Diario de a bordo. Día 45.

No tiene sentido perder la compostura, solo llevo incomunicado un par de días.
Estoy preparado, tengo provisiones y seguro que en unos días todo volverá a la normalidad.
Ayer intenté salir a reparar la nave, pero parece que la compuerta se ha atascado. Menos mal que me traje cosillas para decorar...
En fin, el diario me ayuda un poco a despejarme. A ver qué me encuentro hoy.

Firmado,
+ Un navegante[]
+ Un ornitólogo de rocas[]