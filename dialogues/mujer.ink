...
+ [No puede ser...] -> boda
+ [¡Ya voy!] -> cena

=== boda ===
Ahorré tres sueldos para comprar este anillo, después de la insistente presión de mi madre.
"¿Qué crees que te va a responder si le das un anillo barato? ¡El mío le costó a tu padre un riñón!"
Casi llegamos tarde a la boda por culpa de unas cabras. Había volcado un remolque de heno en el camino, y fue casi imposible resolver aquello. ¡Hasta nos ofrecieron una cabra!
Pero bueno, mereció la pena. 
...
Mereció la pena.

-> END

=== cena ===
¡Ya bajo! ¡Estoy terminando unas cosas!
No quiero comerme la cena fría otra vez. Debería... Debería estar por aquí...
El formulario de inscripción para el programa espacial... Siempre he querido ir al espacio, y creo que sería capaz de conseguirlo.
Pero no sin cenar. En fin, ya lo buscaré luego.
¡Ya voooooy! ¡Qué bien huele!
-> END