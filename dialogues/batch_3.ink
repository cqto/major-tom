Diario de a bordo. Día 500 y algo.
Llevo unas cuantas semanas sin llevar la cuenta de nada, y el reloj se estropeó hace un par de días. Ya no me quedan referencias para saber la hora en la Tierra, y el huerto no está prosperando. 
Dudo mucho que la misión vaya a mejor, pero apuntar lo que veo es lo único que me queda. Si es que me queda algo...

Firmado,
+ Un loco[]
+ Un muerto[]