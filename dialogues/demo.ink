Día terrestre 443, 02:44 hora peninsular. 
Estoy probando los párrafos.
-> pregunta

== pregunta ==
Aquí...
+ [...] -> olvidado
+ [Decir tu nombre] -> nombre

== olvidado ==
Aquí... Bueno, no importa. Sigamos.
-> milagro

== nombre ==
Aquí Major Tom.
-> milagro

== milagro ==
Ha pasado ya casi año y medio desde que perdí el contacto con Control. No pensaba que fuese a durar tanto, es casi un milagro.
-> semillero

== semillero ==
El huerto no pasa por su mejor momento, y el semillero está en las últimas, así que puede que este sea uno de mis últimos registros. 
-> diario

== diario ==
En caso de que alguien encuentre este diario algún día, aquí está todo lo que he ido observando en este largo y solitario viaje. 
-> firma

== firma ==
+ {nombre} [Firmar] -> majortom
+ [Firmar como anónimo] -> anonimo
+ [No hacer nada] -> DONE

== majortom ==
Firmado, Major Tom.
-> DONE

== anonimo ==
Firmado, viajero anónimo.
-> DONE
