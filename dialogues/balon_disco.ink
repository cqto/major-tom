Esta roca es bastante normal, pero se parece un poco a...
+ [Un balón] -> deporte
+ [Una... ¿bola de discoteca?] -> disco
    
=== deporte ===
No es del todo redonda, ni tampoco especialmente bonita. ¡Justo como mi balón de baloncesto!
Estaba tan hecho polvo después de tantas y tantas tardes de entrenamiento que más que un balón parecía una pelota hecha con medias. 
El trabajo en la cafetería no me daba para comprarme uno nuevo, pero pude conseguir otro con la beca de deportes.
Qué tiempos aquellos...
-> END

=== disco ===
Noche tras noche tras noche, no paraba. No había local en la zona en el que no me conocieran.
Vaciaba garrafas, botellas y vasos, y destrozaba los zapatos en cuestión de semanas. Tal vez no era la opción más responsable, pero solo se vive una vez.
Todo eso se acabó cuando empezaron a vetarme de algunos sitios. Tal vez fue una buena idea...
-> END
