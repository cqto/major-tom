La forma de esas estrellas me recuerda...
+ [A mi primera salida al espacio] -> espacio
+ [A la última noche de copas] -> copas

=== espacio ===
A día de hoy sigue siendo difícil de describir.
La inmensidad del vacío... La Tierra a lo lejos...
Me arropó una paz tan grande que todas mis preocupaciones se esfumaron. Por un momento olvidé quién era, qué hacía y todo lo que me había llevado hasta allí.
Era uno con el cosmos.
-> END

=== copas ===
La noche de antes de partir, todo el equipo de la misión salió a celebrarlo.
Entre risas, llantos y algún que otro insulto a los jefes, brindamos por que todo saliese bien.
Y la parte de llegar salió bien. La de volver... Dudo mucho que alguien siga teniendo esperanza.
Yo la perdí hace mucho.
-> END