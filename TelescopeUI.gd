extends Control

@export var target: CharacterBody2D

@onready var reticle := $Reticle


func follow_target(): 
	reticle.position = target.get_global_transform_with_canvas().get_origin()


func _process(_delta):
	follow_target()