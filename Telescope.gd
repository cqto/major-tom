extends CharacterBody2D

const SPEED := 500

@onready var detector_area := $DetectorArea


var is_active := true


func reset():
	position = Vector2.ZERO
	deactivate()


func get_input_direction():
	var dir = Input.get_vector("ui_left", "ui_right", "ui_up", "ui_down")
	if dir == null:
		dir = Vector2.ZERO
	
	return dir


func _ready():
	SignalBus.dialogue_requested.connect(deactivate.unbind(1))
	SignalBus.dialogue_ended.connect(func(): is_active = true)


func deactivate():
	is_active = false


func _process(delta):
	if is_active:
		active_process(delta)

	
func active_process(_delta):
	velocity = get_input_direction() * SPEED
	move_and_slide()
	position = position.clamp(Vector2(-960, -540), Vector2(960, 540))