extends Node2D
class_name BodyBatch

@export var initial_dialogue: StringName
@export var final_dialogue: StringName

var visited_count := 0
var total_count: int


# Called when the node enters the scene tree for the first time.
func _ready():
	total_count = get_child_count()
	SignalBus.body_visited.connect(increase_visited_count)


func start():
	SignalBus.dialogue_requested.emit(initial_dialogue)


func increase_visited_count():
	visited_count += 1

	if visited_count == total_count:
		finish_batch()


func finish_batch():
	await SignalBus.dialogue_ended
	SignalBus.batch_finished.emit(final_dialogue)