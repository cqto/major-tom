extends ColorRect


@onready var anim_player = $AnimationPlayer


signal fade_finished


func fade_in():
	anim_player.play("Fade")
	await anim_player.animation_finished
	fade_finished.emit()


func fade_out():
	anim_player.play_backwards("Fade")
	await anim_player.animation_finished
	fade_finished.emit()
