extends Node2D

@export var body_data: CelestialBody

@onready var celestial_body := $CelestialBody


# Called when the node enters the scene tree for the first time.
func _ready():
	celestial_body.set_data(body_data)
	