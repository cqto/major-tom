extends RichTextLabel


func set_and_start(new_text: String):
	visible_characters = 0
	text = new_text


func increment_visible_characters_by_one():
	if(visible_characters < get_total_character_count()):
		visible_characters += 1


func set_max_visible_characters():
	visible_characters = get_total_character_count()


func is_totally_visible() -> bool:
	return visible_characters == get_total_character_count()