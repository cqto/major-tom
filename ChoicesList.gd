extends VBoxContainer


@export var button_template: Button = Button.new()

@export var choices: Array = []:
	set(value):
		choices = value
		clear_buttons()
		add_choice_buttons()


signal choice_selected(choice_index: int)


func clear_buttons():
	for child in get_children():
		child.queue_free()

func create_choice_button(choice_index: int) -> Button:
	var btn = button_template.duplicate()
	btn.text = choices[choice_index]
	btn.pressed.connect(select_choice.bind(choice_index))
	return btn
	

func add_choice_buttons():	
	for i in choices.size():
		add_child(create_choice_button(i))


func select_choice(choice_index: int):
	choice_selected.emit(choice_index)
