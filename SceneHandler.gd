extends Node2D

const BATCH_PATH = "res://body-batches/%s.tscn"

var batches := ["Batch1", "Batch2", "Batch3"]
var next_batch_index := -1

@onready var UI = $UI
@onready var transition = %SceneTransition
@onready var telescope = $Telescope
@onready var end_screen = %END

var current_batch_node: BodyBatch


func _ready():
	SignalBus.batch_finished.connect(transition_to_next_batch)
	load_next_batch()


func load_next_batch():
	next_batch_index += 1
	if (next_batch_index == len(batches)):
		end_screen.show()
		
	else:
		UI.show()
		telescope.reset()
		current_batch_node = load(BATCH_PATH % batches[next_batch_index]).instantiate()
		add_child(current_batch_node)
		transition.fade_out()
		await transition.fade_finished
		current_batch_node.start()


func transition_to_next_batch(final_dialogue: StringName):
	transition.fade_in()
	await transition.fade_finished
	SignalBus.dialogue_requested.emit(final_dialogue)
	await SignalBus.dialogue_ended
	UI.hide()
	remove_child(current_batch_node)
	load_next_batch()


