extends CanvasLayer


@onready var telescope_ui = $TelescopeUI
@onready var dialogue_ui = $DialogueUI


# Called when the node enters the scene tree for the first time.
func _ready():
	SignalBus.dialogue_requested.connect(show_dialogue)
	SignalBus.dialogue_ended.connect(hide_dialogue)


func show_dialogue(dialogue_path: StringName):
	print("d")
	telescope_ui.hide()
	dialogue_ui.show()
	dialogue_ui.load_and_start(dialogue_path) 


func hide_dialogue():
	dialogue_ui.hide()
	telescope_ui.show()