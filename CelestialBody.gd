extends Resource
class_name CelestialBody

const PATH_TEMPLATE = "res://dialogues/%s.ink.json"

@export var texture: Texture2D
@export var shape: Shape2D
@export var first_visit_dialogue: StringName:
	set(value):
		first_visit_dialogue = PATH_TEMPLATE % value
@export var visited_dialogue: StringName:
	set(value):
		visited_dialogue = PATH_TEMPLATE % value
		