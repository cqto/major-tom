extends Node

signal dialogue_requested(dialogue_path: StringName)
signal dialogue_ended

signal body_visited
signal batch_finished(final_dialogue_path: StringName)