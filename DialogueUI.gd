extends Control

var _ink_player = InkPlayer.new()

@onready var character_timer := $CharacterTimer
@onready var text_label := %Text 
@onready var choices_list := $ChoicesList

var is_active := false

signal continue_requested


func load_and_start(dialogue_path: StringName):
	_ink_player.ink_file = load(str(dialogue_path))
	
	# It's recommended to load the story in the background. On platforms that
	# don't support threads, the value of this variable is ignored.
	_ink_player.loads_in_background = true
		
	# Creates the story. 'loaded' will be emitted once Ink is ready
	# continue the story.
	_ink_player.create_story()


func _input(event):
	if is_active and event.is_action_pressed("select_body"):
		continue_requested.emit()


func _ready():
	# Adds the player to the tree.
	add_child(_ink_player)

	_ink_player.connect("loaded", Callable(self, "_story_loaded"))
	_ink_player.connect("continued", Callable(self, "_continued"))
	_ink_player.connect("prompt_choices", _prompt_choices)
	_ink_player.connect("ended", _ended)

	choices_list.choice_selected.connect(_select_choice)
	continue_requested.connect(continue_or_show_full_text)

	# Text label control
	character_timer.timeout.connect(text_label.increment_visible_characters_by_one)


# ############################################################################ #
# Signal Receivers
# ############################################################################ #

func _story_loaded(successfully: bool):
	if !successfully:
		return

	# _observe_variables()
	# _bind_externals()d

	# Here, the story is started immediately, but it could be started
	# at a later time.
	is_active = true
	_ink_player.continue_story()


func continue_or_show_full_text():
	if text_label.is_totally_visible():
		_ink_player.continue_story()	
	else:
		text_label.set_max_visible_characters()


func _continued(text, _tags):
	text_label.set_and_start(text)
	character_timer.start()


# ############################################################################ #
# Private Methods
# ############################################################################ #

func _prompt_choices(choices):
	if !choices.is_empty():
		choices_list.choices = choices.map(func(choice): return choice.text)
		choices_list.show()


func _ended():
	if is_active:
		SignalBus.dialogue_ended.emit()
		is_active = false
		print("ended")


func _select_choice(index):
	print(index)
	choices_list.hide()

	_ink_player.choose_choice_index(index)
	_ink_player.continue_story()


# Uncomment to bind an external function.
#
# func _bind_externals():
# 	_ink_player.bind_external_function("<function_name>", self, "_external_function")
#
#
# func _external_function(arg1, arg2):
# 	pass


# Uncomment to observe the variables from your ink story.
# You can observe multiple variables by putting adding them in the array.
# func _observe_variables():
# 	_ink_player.observe_variables(["var1", "var2"], self, "_variable_changed")
#
#
# func _variable_changed(variable_name, new_value):
# 	print("Variable '%s' changed to: %s" %[variable_name, new_value])

